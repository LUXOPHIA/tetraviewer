﻿unit Main;

interface //#################################################################### ■

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  System.Math.Vectors,
  FMX.Types3D, FMX.Controls3D, FMX.Objects3D, FMX.Menus, FMX.Viewport3D,
  LUX.Brep.Cell.TetraFlip.D3, LUX.Brep.Cell.TetraFlip.D3.FMX,
  Core, FMX.MaterialSources;

type
  TForm1 = class(TForm)
    MainMenu1: TMainMenu;
    Viewport3D1: TViewport3D;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    Dummy1: TDummy;
    Dummy2: TDummy;
    Camera1: TCamera;
    Grid3D1: TGrid3D;
    Light1: TLight;
    LightMaterialSource1: TLightMaterialSource;
    OpenDialog1: TOpenDialog;
    SaveDialog1: TSaveDialog;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Viewport3D1MouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Single);
    procedure Viewport3D1MouseMove(Sender: TObject; Shift: TShiftState; X, Y: Single);
    procedure Viewport3D1MouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Single);
    procedure MenuItem2Click(Sender: TObject);
    procedure MenuItem3Click(Sender: TObject);
  private
    { private 宣言 }
    _MouseS :TShiftState;
    _MouseP :TPointF;
  public
    { public 宣言 }
    _Model :TTetraModel3D;
    _Edges :TDelaEdges;
  end;

var
  Form1: TForm1;

implementation //############################################################### ■

{$R *.fmx}

//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& private

//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& public

//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

procedure TForm1.FormCreate(Sender: TObject);
begin
     _MouseS := [];

     _Model := TTetraModel3D.Create;

     _Edges := TDelaEdges.Create( Self );

     with _Edges do
     begin
          Parent      := Viewport3D1;
          Material    := LightMaterialSource1;
          TetraModel  := _Model;
          EdgeRadius  := 0.002;
          Scale.Point := TPOint3D.Create( 10, 10, 10 );
     end;
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
     _Model.Free;
end;

//------------------------------------------------------------------------------

procedure TForm1.MenuItem2Click(Sender: TObject);
begin
     with OpenDialog1 do
     begin
          if Execute then
          begin
               _Model.LoadFromFile( FileName );

               _Edges.MakeModel;

               Viewport3D1.Repaint;
          end;
     end;
end;

procedure TForm1.MenuItem3Click(Sender: TObject);
begin
     with SaveDialog1 do
     begin
          if Execute then
          begin
               _Model.SaveToFile( FileName );

               _Edges.MakeModel;

               Viewport3D1.Repaint;
          end;
     end;
end;

//------------------------------------------------------------------------------

procedure TForm1.Viewport3D1MouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
     _MouseS := Shift;
     _MouseP := TPointF.Create( X, Y );
end;

procedure TForm1.Viewport3D1MouseMove(Sender: TObject; Shift: TShiftState; X, Y: Single);
var
   P :TPointF;
begin
     if ssLeft in _MouseS then
     begin
          P := TPointF.Create( X, Y );

          with Dummy1.RotationAngle do Y := Y + ( P.X - _MouseP.X );
          with Dummy2.RotationAngle do X := X - ( P.Y - _MouseP.Y );

          _MouseP := P;
     end;
end;

procedure TForm1.Viewport3D1MouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
     Viewport3D1MouseMove( Sender, Shift, X, Y );

     _MouseS := [];
end;

end. //######################################################################### ■
